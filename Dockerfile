FROM node:12.12.0-alpine
MAINTAINER <prashu.c@zolostays.com>

RUN mkdir -p /opt/app
RUN apk add --no-cache libc6-compat
ENV NODE_ENV production

WORKDIR /opt/app

RUN apk add --no-cache git && \
    npm install i -g pm2

COPY package.json /opt/app
COPY yarn.lock /opt/app

RUN yarn install --ignore-optional

COPY . /opt/app

RUN yarn run build

RUN npx next telemetry disable

RUN addgroup -g 1001 -S nodejs
RUN adduser -S zeassetz -u 1001

USER zeassetz

CMD [ "pm2-runtime", "start", "npm", "--", "start" ]
