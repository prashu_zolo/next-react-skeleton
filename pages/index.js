import React from 'react'
import {H1, H2, FlexCol} from '../components/globals'

export default function Home() {
  return (
    <FlexCol>
      <H1>Star Wars: The Last Jedi</H1>
      <H2>
        Only General Leia Organa's band of RESISTANCE fighters stand against the rising tyranny,
        certain that Jedi Master Luke Skywalker will return and restore a spark of hope to the fight.
        But the Resistance has been exposed.
      </H2>
    </FlexCol>
  );
}
