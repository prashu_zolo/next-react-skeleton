// @flow
import styled from 'styled-components';

export const A = styled.a`
  display: flex;
  align-items: center;
  flex: none;
`;

export const StyledButton = styled.button`
  font-weight: bold;
  color: #ffffff;
  border-radius: 2px;
  background: #1f3231;
  cursor: pointer;
  -webkit-display: none;
  line-height: 1.36;
  display: flex;
  flex: none;
  align-items: center;
  justify-content: center;
  opacity: ${props => (props.disabled ? '0.6' : '1')};
  font-size: ${props => (props.size === 'large' ? '16px' : '14px')};
  padding: ${props => (props.size === 'large' ? '16px 80px': '16px 34px')};
`;


export const StyledWhiteHollowButton = styled(StyledButton)`
  color: #343434;
  background-color: #ffffff;
  border: solid 1px #1f3231;
`;
