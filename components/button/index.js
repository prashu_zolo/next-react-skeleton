// @flow
import * as React from 'react';
import {StyledButton} from './style';


const handleProps = (Component, props: Props) => {
  const { children, disabled, isLoading, size } = props;
  return (
    <Component disabled={disabled || isLoading} size={size}>
      {children}
    </Component>
  );
};

type Props = {
  children: React.Node,
  disabled?: boolean,
  isLoading?: boolean,
  size?: 'small'
};

export const Button = (props: Props) => handleProps(StyledButton, props);
export const WhiteHollowButton = (props: Props) => handleProps(StyledButton, props);
