export const CONFIG = Object.freeze({
  NODE_ENV: process.env.NODE_ENV,
  SERVER_URL: process.env.APP_SERVER_URL,
});
